## Info

- A `routes.js` file was created and includes a nice list of all endpoints.
- A "service" (control) layer houses the hanlders for endpoints split by the different entities and resources.
- Some changes were made to the models used to generate the table schema. The `paid` and `balance` columns allowed for null values and didn't include default values. I thought it made sense to add default values to these fields to remove the burdon of having to consider `null`'s and reduce chances of bugs as the schema is simpler.

### Assumptions

- The deposit endpoint specification wasn't entirely clear for me so would seek further clarification. My assumption was described in the handler function's JSDoc comment.
- The deposit endpoint received the profile ID (userId) as a parameter of the URL which lead me to believe the endpoint would not be authenticated. My assumption would require confirming.
- I assumed I had the right to modify the provided database schema as ideally there would be one service accessing (this app/service) but realistically in a large system would require checking if anything currently relies on the existing schema.

## Improvements

- I would create constant variables to use for the ENUM columns in the database for easier refactoring purposes.
- Validation of payload fields and query parameters was performed maually but in a larger project I would use the help of a schema validator such as `zod`, `yup` or `ajv`.
- There is some occurances of duplicate code blocks which may make sense to abstract. Duplicate single lines may not suite abstracting but indicate a code flow arrangement refactor could be made to remove. I opted to duplicate for readability purposes by keeping the flow mostly flat with conditionals diverging from the happy path.
- In a larger project it would be beneficial to have a "store" layer similar to the "service" layer that can house the access patterns to the database. All the sequalize queries would be moved from the service layer to the store. This would mean simpler/smaller handler functions and easier testing of the isloated query functions.
- I would end to end test the API's using supertest. A pattern I like for integration testing with a live database with transactions is running the tests inside a transaction which isolates changes between parallel tests and changes can all be rolled back at the end of the test to return to the same consistent starting point.
- Typescript integration for a better development experiance but didn't want to use time setting up build tools as the project was already bootstrapped.
- There is a mix of parameter casing in route parameters that I would make consistent throughout the application but treated the readme as a specification.
- A logging tool such as `pino` would be beneficial in all layers of the application for high level feedback on the system.
- Routes would probably expand from a single file to a directory grouped by resource or entity files that would allow for finer middleware management and organisation.
- Helmet middleware or similar should be considered for added security.
- As the data increases, the admin endpoints performance may degrade as the joins in the query do not scale well. If consistent performance of the response is required, the stats can be calculated periodically and cached but results would not be real-time and instead outdated.
- Error handling is very generic as well as error responses from the endpoints. Granular error checking would allow specific error logs and responses.
