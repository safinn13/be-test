const { Op } = require('sequelize')

/**
 * @returns unpaid jobs by profile
 * Only includes jobs on active ('in_progress' status) contracts
 */
const getUnpaidJobsByProfile = async (req, res) => {
  const { Job, Contract } = req.app.get('models')
  const sequelize = req.app.get('sequelize')
  const { id: profileId, type: profileType } = req.profile

  try {
    const jobs = await Job.findAll({
      attributes: Object.keys(Job.getAttributes()),
      include: {
        model: Contract,
        attributes: [] // Don't include any of contracts attributes
      },
      where: {
        [Op.and]: [
          {
            paid: false
          },
          sequelize.where(sequelize.col('Contract.status'), 'in_progress'), // 'in_progress' status = active contract
          {
            [Op.or]: [
              sequelize.where(
                sequelize.col('Contract.ContractorId'),
                profileId
              ),
              sequelize.where(sequelize.col('Contract.ClientId'), profileId)
            ]
          }
        ]
      }
    })

    res.json(jobs)
  } catch (err) {
    return res.status(500).end()
  }
}

/**
 * Allows a client to pay for a job.
 * Idempotent response on already paid jobs.
 * @returns {Job}
 */
const payForJob = async (req, res) => {
  const { Job, Contract, Profile } = req.app.get('models')
  const sequelize = req.app.get('sequelize')
  const { id: profileId } = req.profile
  const { job_id } = req.params

  try {
    await sequelize.transaction(async (t) => {
      // Don't require profile until after making sure the caller is the client.
      // But will require for balance check so join all entities in single query.
      const job = await Job.findOne({
        include: {
          model: Contract,
          include: {
            model: Profile,
            as: 'Client'
          }
        },
        where: [
          { id: job_id },
          sequelize.where(sequelize.col('Contract.Client.id'), profileId)
        ],
        transaction: t
      })
      if (!job) return res.status(404).end()

      // Job is already paid, idempotently return successfully.
      if (job.paid) {
        // Only return job entity
        const { Contract: c, ...rest } = job.toJSON()
        return res.json(rest)
      }

      if (job.Contract.Client.balance < job.price)
        return res.status(400).json({ messge: 'insufficient balance' })

      // Decrement job price from the balance of the client.
      const dec = Profile.decrement(
        { balance: job.price },
        { where: { id: job.Contract.Client.id }, transaction: t }
      )
      // Increment the contractors balance by the job price.
      const inc = Profile.increment(
        {
          balance: job.price
        },
        { where: { id: job.Contract.ContractorId }, transaction: t }
      )
      // Mark the job as paid.
      job.paid = true
      job.paymentDate = new Date().toISOString()
      const pay = job.save({ transaction: t })

      // Any failures will immediately reject and roll back the transaction.
      await Promise.all([inc, dec, pay])

      // Only return job entity
      const { Contract: c, ...rest } = job.toJSON()
      res.json(rest)
    })
  } catch (error) {
    console.log(error)
    // Can only be reached if any DB errors occur as all criteria are met and therefore server error.
    res.status(500).end()
  }
}

module.exports = {
  getUnpaidJobsByProfile,
  payForJob
}
