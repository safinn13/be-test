const { getSequalizeTimePeriodCondition } = require('../store/utils')

/**
 *
 * @param req.query.start {string}
 * @param req.query.end {string}
 * @returns the profession with the most paid jobs in a specified period
 */
const bestProfession = async (req, res) => {
  const { Job, Contract, Profile } = req.app.get('models')
  const sequelize = req.app.get('sequelize')
  const { start, end } = req.query

  const periodCondition = getSequalizeTimePeriodCondition(
    'createdAt',
    Date.parse(start),
    Date.parse(end)
  )

  try {
    const sumJobPriceByProfession = await Job.findAll({
      attributes: [
        [sequelize.col('Contract.Contractor.profession'), 'profession'],
        [sequelize.fn('sum', sequelize.col('price')), 'profession_sum']
      ],
      include: {
        model: Contract,
        attributes: [],
        include: {
          model: Profile,
          as: 'Contractor'
        }
      },
      where: { paid: true, ...periodCondition },
      group: sequelize.col('Contract.Contractor.profession')
    })

    const bestProfession = sumJobPriceByProfession.reduce((prev, curr) =>
      prev.profession_sum > curr.profession_sum ? prev : curr
    )

    return res.json(bestProfession)
  } catch (err) {
    res.status(500).end()
  }
}

/**
 *
 * @param req.query.start {string}
 * @param req.query.end {string}
 * @param req.query.limit {string}
 * @returns a list of clients ordered by most paid jobs in the selected period
 */
const bestClients = async (req, res) => {
  const { Job, Contract, Profile } = req.app.get('models')
  const sequelize = req.app.get('sequelize')
  const { start, end } = req.query
  let limit = req.query.limit

  const periodCondition = getSequalizeTimePeriodCondition(
    'paymentDate',
    Date.parse(start),
    Date.parse(end)
  )

  // If limit is not an integer, default to 2.
  if (!Number.isInteger(Number(limit))) {
    limit = 2
  }

  try {
    const sumJobPaidByClient = await Job.findAll({
      attributes: [
        [sequelize.col('Contract.Client.id'), 'id'],
        [sequelize.literal('firstName || " " || lastName'), 'fullName'],
        [sequelize.fn('sum', sequelize.col('price')), 'paid']
      ],
      include: {
        model: Contract,
        attributes: [],
        include: {
          model: Profile,
          as: 'Client'
        }
      },
      where: { paid: true, ...periodCondition },
      group: sequelize.col('Contract.Client.id'),
      limit,
      order: [[sequelize.col('paid'), 'desc']]
    })

    return res.json(sumJobPaidByClient)
  } catch (err) {
    console.log(err)
    res.status(500).end()
  }
}

module.exports = {
  bestProfession,
  bestClients
}
