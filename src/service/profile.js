const { Op } = require('sequelize')

/**
 * Deposits money into a clients account.
 * amount cannot exceed 25% of the sum of the current outstanding jobs
 * e.g. sum of outstanding jobs = 60. 25% = 15. cannot deposit more than 15.
 * @param req.body.amount {Number}
 * @returns {Profile}
 */
const deposit = async (req, res) => {
  const { Job, Contract, Profile } = req.app.get('models')
  const sequelize = req.app.get('sequelize')
  const { userId } = req.params
  const { amount } = req.body

  // validate amount. must be a number
  if (typeof amount !== 'number' || isNaN(amount)) {
    return res.status(400).json({ message: 'invalid amount' }) // Bad request
  }

  try {
    await sequelize.transaction(async (t) => {
      const profile = await Profile.findOne({
        where: { id: userId, type: 'client' },
        transaction: t
      })

      // Profile wasn't found. ID did not match or was not a client.
      if (!profile) {
        return res.status(404).end()
      }

      const outstandingJobsTotalPrice = await Job.sum('price', {
        include: { model: Contract, attributes: [] },
        where: [
          sequelize.where(sequelize.col('Contract.ClientId'), userId),
          { paid: false }
        ],
        transaction: t
      })

      // null may be returned if there are no outstanding jobs
      // 0 will also be caught here.
      // Both cases result in no deposit being allowed.
      if (!outstandingJobsTotalPrice) {
        return res
          .status(405) // Not allowed
          .json({
            message: 'deposit not required as no jobs require payment'
          })
      }

      const maxDeposit = outstandingJobsTotalPrice * 0.25

      if (amount > maxDeposit) {
        return res
          .status(405) // Not allowed
          .json({ message: `cannot deposit more than ${maxDeposit}` })
      }

      await Profile.increment(
        { balance: amount },
        { where: { id: userId }, transaction: t }
      )

      // update profile after deposit so it can be returned
      await profile.reload()

      res.json(profile)
    })
  } catch (err) {
    res.status(500).end()
  }
}

module.exports = {
  deposit
}
