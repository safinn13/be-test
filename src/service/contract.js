const { Op } = require('sequelize')

/**
 * @returns contract by id
 */
const getContractByID = async (req, res) => {
  const { Contract } = req.app.get('models')
  const { id } = req.params
  const { id: profileId } = req.profile

  try {
    const contract = await Contract.findOne({
      where: {
        id,
        [Op.or]: [{ ContractorId: profileId }, { ClientId: profileId }]
      }
    })
    if (!contract) return res.status(404).end()
    res.json(contract)
  } catch (err) {
    return res.status(500).end()
  }
}

/**
 * @returns contracts by profile
 * Excludes 'terminated' contracts
 */
const getContractByProfile = async (req, res) => {
  const { Contract } = req.app.get('models')
  const { id: profileId } = req.profile

  try {
    const contracts = await Contract.findAll({
      where: {
        status: {
          [Op.ne]: 'terminated'
        },
        [Op.or]: [{ ContractorId: profileId }, { ClientId: profileId }]
      }
    })
    res.json(contracts)
  } catch (err) {
    return res.status(500).end()
  }
}

module.exports = {
  getContractByID,
  getContractByProfile
}
