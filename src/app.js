const express = require('express')
const bodyParser = require('body-parser')
const { sequelize } = require('./model')
const routes = require('./routes')

const app = express()

// Middleware
app.use(bodyParser.json())

// Global items on express app
app.set('sequelize', sequelize)
app.set('models', sequelize.models)

// Register all routes
app.use(routes)

module.exports = app
