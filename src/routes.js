const express = require('express')
const { getProfile } = require('./middleware/getProfile')
const contractService = require('./service/contract')
const jobsService = require('./service/jobs')
const profileService = require('./service/profile')
const adminService = require('./service/admin')

const router = express.Router()

// Admin endpoints
router.get('/admin/best-profession', adminService.bestProfession)
router.get('/admin/best-clients', adminService.bestClients)

// Profile endpoints
// As profile id is provided as a parameter, it is assumed that auth is not required.
router.post('/balances/deposit/:userId', profileService.deposit)

// "auth" middlware applied to all endpoints bellow
router.use(getProfile)

// Contract endpoints
router.get('/contracts/:id', contractService.getContractByID)
router.get('/contracts', contractService.getContractByProfile)

// Job endpoints
router.get('/jobs/unpaid', jobsService.getUnpaidJobsByProfile)
router.post('/jobs/:job_id/pay', jobsService.payForJob)

module.exports = router
