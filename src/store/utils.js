const { Op } = require('sequelize')

/**
 * @params {(date|number)} [start]
 * @params {(date|number)} [end]
 * @returns a sequalize condition that checks a certain field falls between the
 * start and end parameters inclusively. Supports only one boundary being provided
 * for filtering only one side.
 */
function getSequalizeTimePeriodCondition(field, start, end) {
  const condition = {
    [field]: {}
  }

  if (start) condition[field][Op.gte] = start
  if (end) condition[field][Op.lte] = end

  return condition
}

module.exports = {
  getSequalizeTimePeriodCondition
}
